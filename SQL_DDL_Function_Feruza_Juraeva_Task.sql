INSERT INTO dvdrentalschema.language (name) VALUES
('English'), ('French'), ('German'), ('Italian'), ('Klingon');

INSERT INTO dvdrentalschema.film (title, description, release_year, language_id,
rental_duration, rental_rate, length, replacement_cost, rating)
VALUES 
('Space Odyssey', 'A journey through space.', 1968, 1, 7, 3.99, 120, 12.99, 'PG'),
('The Godfather', 'An Italian-American mafia family story.', 1972, 1, 5, 4.99, 175, 15.99, 'R'),
('Pulp Fiction', 'A series of interconnected crime stories.', 1994, 1, 6, 3.99, 154, 14.99, 'R');

INSERT INTO dvdrentalschema.category (name) VALUES
('Sci-Fi'), ('Drama'), ('Action'), ('Comedy'), ('Horror');

INSERT INTO dvdrentalschema.film_category (film_id, category_id)
VALUES 
(1, 1),(2, 2),(3, 3); 

INSERT INTO dvdrentalschema.inventory (film_id, store_id, last_update)
VALUES 
(1, 1, CURRENT_TIMESTAMP),
(2, 1, CURRENT_TIMESTAMP);

INSERT INTO dvdrentalschema.customer (store_id, first_name, last_name,
email, address_id, active, create_date)
VALUES 
(1, 'John', 'Doe', 'johndoe@example.com', 1, TRUE, CURRENT_DATE),
(1, 'Jane', 'Smith', 'janesmith@example.com', 2, TRUE, CURRENT_DATE);

INSERT INTO dvdrentalschema.rental (inventory_id, customer_id, rental_date,
return_date, staff_id, last_update)
VALUES 
(1, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP + INTERVAL '7 days', 1, CURRENT_TIMESTAMP),
(2, 2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP + INTERVAL '5 days', 1, CURRENT_TIMESTAMP);

INSERT INTO dvdrentalschema.payment (customer_id, staff_id,
rental_id, amount, payment_date)
VALUES 
(1, 1, 1, 3.99, CURRENT_TIMESTAMP),
(2, 1, 2, 4.99, CURRENT_TIMESTAMP);
-- complete inserting 

SELECT * FROM dvdrentalschema.category;
SELECT * FROM dvdrentalschema.film;
SELECT * FROM dvdrentalschema.customer;
SELECT * FROM dvdrentalschema.film_category;
SELECT * FROM dvdrentalschema.inventory;
SELECT * FROM dvdrentalschema.language;
SELECT * FROM dvdrentalschema.payment;
SELECT * FROM dvdrentalschema.rental;
SELECT * FROM dvdrentalschema.staff;
SELECT * FROM dvdrentalschema.store;

-- creating sales_revenue_by_category_qtr view
CREATE OR REPLACE VIEW dvdrentalschema.sales_revenue_by_category_qtr AS
SELECT 
    c.name AS category_name,
    SUM(p.amount) AS total_revenue
FROM 
    dvdrentalschema.payment p
JOIN 
    dvdrentalschema.rental r ON p.rental_id = r.rental_id
JOIN 
    dvdrentalschema.inventory i ON r.inventory_id = i.inventory_id
JOIN 
    dvdrentalschema.film f ON i.film_id = f.film_id
JOIN 
    dvdrentalschema.film_category fc ON f.film_id = fc.film_id
JOIN 
    dvdrentalschema.category c ON fc.category_id = c.category_id
WHERE 
    EXTRACT(QUARTER FROM p.payment_date) = EXTRACT(QUARTER FROM CURRENT_DATE)
    AND EXTRACT(YEAR FROM p.payment_date) = EXTRACT(YEAR FROM CURRENT_DATE)
GROUP BY 
    c.name
HAVING 
    SUM(p.amount) > 0;
   
-- creating get_sales_revenue_by_category_qtr function
 CREATE OR REPLACE FUNCTION dvdrentalschema.get_sales_revenue_by_category_qtr(quarter INTEGER)
RETURNS TABLE(category_name VARCHAR, total_revenue NUMERIC) AS $$
BEGIN
    RETURN QUERY 
    SELECT 
        c.name,
        SUM(p.amount)
    FROM 
        dvdrentalschema.payment p
    JOIN 
        dvdrentalschema.rental r ON p.rental_id = r.rental_id
    JOIN 
        dvdrentalschema.inventory i ON r.inventory_id = i.inventory_id
    JOIN 
        dvdrentalschema.film f ON i.film_id = f.film_id
    JOIN 
        dvdrentalschema.film_category fc ON f.film_id = fc.film_id
    JOIN 
        dvdrentalschema.category c ON fc.category_id = c.category_id
  --  WHERE 
    --    EXTRACT(QUARTER FROM p.payment_date) = quarter
      --  AND EXTRACT(YEAR FROM p.payment_date) = EXTRACT(YEAR FROM CURRENT_DATE)
        WHERE 
    EXTRACT(QUARTER FROM p.payment_date) = EXTRACT(QUARTER FROM CURRENT_DATE)
    AND EXTRACT(YEAR FROM p.payment_date) = EXTRACT(YEAR FROM CURRENT_DATE)
    --
    GROUP BY 
        c.name
    HAVING 
        SUM(p.amount) > 0;
END;
$$ LANGUAGE plpgsql;


-- creating new_movie procedure function
CREATE OR REPLACE FUNCTION dvdrentalschema.new_movie(movie_title VARCHAR) RETURNS VOID AS $$
DECLARE
    lang_id INT;
BEGIN
    -- Check if Klingon language exists
    SELECT INTO lang_id language_id FROM dvdrentalschema.language WHERE name = 'Klingon';

    IF NOT FOUND THEN
        RAISE EXCEPTION 'Language Klingon does not exist.';
    END IF;

    -- Inserting new movie
    INSERT INTO dvdrentalschema.film (title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE)::INTEGER, lang_id);
END;
$$ LANGUAGE plpgsql;


SELECT * FROM dvdrentalschema.sales_revenue_by_category_qtr;

SELECT * FROM dvdrentalschema.get_sales_revenue_by_category_qtr(4);


-- using new_movie procedure function
SELECT  dvdrentalschema.new_movie('Star Trek: The Klingon Encounter');

-- Verifing the insertion
SELECT * FROM dvdrentalschema.film WHERE title = 'Star Trek: The Klingon Encounter';



