-- creating DVD rental database which will be used as foundation for task1, task2, task3 solution
CREATE DATABASE dvdrental;

CREATE SCHEMA dvdrentalschema;
-- Creating language table
CREATE TABLE dvdrentalschema.language (
    language_id SERIAL PRIMARY KEY,
    name VARCHAR(20) NOT NULL
);


-- Creating film table
CREATE TABLE dvdrentalschema.film (
    film_id SERIAL PRIMARY KEY,
    title VARCHAR(255)NOT NULL,
    description TEXT ,
    release_year INTEGER NOT NULL,
    language_id INT REFERENCES dvdrentalschema.language(language_id),
    rental_duration SMALLINT,
    rental_rate DECIMAL(4,2)NOT NULL CHECK (rental_rate >=0),
    length SMALLINT,
    replacement_cost DECIMAL(5,2) NOT NULL CHECK (rental_rate >=0),
    rating VARCHAR(5)
);


-- Creating category table
CREATE TABLE dvdrentalschema.category (
    category_id SERIAL PRIMARY KEY,
    name VARCHAR(25) NOT NULL
);

-- Creating film_category table
CREATE TABLE dvdrentalschema.film_category (
    film_id INT REFERENCES dvdrentalschema.film(film_id),
    category_id INT REFERENCES dvdrentalschema.category(category_id),
    PRIMARY KEY (film_id, category_id)
);


-- Creating customer table
CREATE TABLE dvdrentalschema.customer (
    customer_id SERIAL PRIMARY KEY,
    store_id INT NOT NULL,
    first_name VARCHAR(45)NOT NULL,
    last_name VARCHAR(45) NOT NULL,
    email VARCHAR(50) NOT NULL,
    address_id INT NOT NULL,
    active BOOLEAN,
    create_date DATE NOT NULL,
    last_update TIMESTAMP
);

-- Creating payment table
CREATE TABLE dvdrentalschema.payment (
    payment_id SERIAL PRIMARY KEY,
    customer_id INT REFERENCES dvdrentalschema.customer(customer_id),
    staff_id INT NOT NULL,
    rental_id INT NOT NULL,
    amount DECIMAL(5,2)NOT NULL CHECK (amount >= 0),
    payment_date TIMESTAMP
);

-- Creating inventory table
CREATE TABLE dvdrentalschema.inventory (
    inventory_id SERIAL PRIMARY KEY,
    film_id INT REFERENCES dvdrentalschema.film(film_id),
    store_id INT  NOT NULL,
    last_update TIMESTAMP
);

-- Creating rental table
CREATE TABLE dvdrentalschema.rental (
    rental_id SERIAL PRIMARY KEY,
    rental_date TIMESTAMP,
    inventory_id INT REFERENCES dvdrentalschema.inventory(inventory_id),
    customer_id INT REFERENCES dvdrentalschema.customer(customer_id),
    return_date TIMESTAMP,
    staff_id INT NOT NULL,
    last_update TIMESTAMP
);

-- Creating staff table
CREATE TABLE dvdrentalschema.staff (
    staff_id SERIAL PRIMARY KEY,
    first_name VARCHAR(45) NOT NULL,
    last_name VARCHAR(45) NOT NULL,
    address_id INT NOT NULL,
    email VARCHAR(50) NOT NULL,
    store_id INT NOT NULL,
    active BOOLEAN,
    username VARCHAR(16) NOT NULL,
    password VARCHAR(40) NOT NULL,
    last_update TIMESTAMP
);

-- Creating store table
CREATE TABLE dvdrentalschema.store (
    store_id SERIAL PRIMARY KEY,
    manager_staff_id INT REFERENCES dvdrentalschema.staff(staff_id),
    address_id INT NOT NULL,
    last_update TIMESTAMP
);
